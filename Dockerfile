FROM alpine:latest

MAINTAINER Hirokazu Yoshida <y.hirokazu@gmail.com>

WORKDIR "/opt"

ADD .docker_build/solver-jp /opt/bin/solver-jp

CMD ["/opt/bin/solver-jp"]
